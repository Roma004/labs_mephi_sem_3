#include "testing.hpp"

bool Testing::process_test(str category_name, str subcategory_name, str name, test t)  {
    try {
        std::cout << "    \033[1;37m" << name << "\033[0m: " << std::flush;
        t();
        std::cout << "\033[0;32mOK\033[0m\n";
        return true;
    } catch (Testing::Exception error) {
        std::cout << "\033[0;31mFAILURE\033[0m\n";
        std::cout << error.what() << "\n";
        return false;
    }
}

std::pair<int, int> Testing::process_category(str name) {
    std::cout << "\033[1;34m\n" << name << " tests\033[0m\n" << std::flush;
    
    int test_counter = 0;
    int total = ctgs[name].size();
    str prev_subctg = "";
    for (auto &&[subctg_name, test_name, test]: ctgs[name]) {
        if (subctg_name != prev_subctg)
            std::cout << "  \033[1;35m" << subctg_name << "\033[0m\n" << std::flush;
        prev_subctg = subctg_name;

        test_counter += process_test(
            name,
            subctg_name,
            test_name,
            test
        );
    }

    std::cout << "\033[1;37m---------------------------------\033[0m\n";
    if (test_counter != total) {
        std::cout << "\033[0;31m" << test_counter << "\033[0m out of \033[0;32m" << total << "\033[0m tests passed!\n";
    } else {
        std::cout << "\033[0;32mAll tests in category `" << name << "` passed!\033[0m\n";
    }

    return {test_counter, total};
}

void Testing::add_category(str name) {
    if (ctgs.find(name) == ctgs.end()) {
        categories_order.push_back({name, {}});
    }
    ctgs[name] = {};
}

void Testing::add_category(str name, category subctgs) {
    if (ctgs.find(name) == ctgs.end()) {
        categories_order.push_back({name, {}});
    }
    ctgs[name] = {};
    for (auto &&[subctg_name, tests]: subctgs) {
        for (auto &&[test_name, test]: tests) {
            ctgs[name].push_back({subctg_name, test_name, test});
        }
    }
}

void Testing::add_subcategory(str category_name, str name, subcategory t) {
    for (auto &&[test_name, test]: t) {
        ctgs[category_name].push_back({name, test_name, test});
    }
}

void Testing::add_test(str category_name, str subcategory_name, str name, test t) {
    ctgs[category_name].push_back({subcategory_name, name, t});
}

void Testing::process() {
    std::cout << "\033[1;36mTesting started!\033[0m\n";

    std::map<str, std::pair<int, int>> report;
    for (auto &&[name, category]: ctgs) report[name] = {0, 0};

    for (auto &&[name, category]: ctgs) {
        auto res = process_category(name);
        report[name].first += res.first;
        report[name].second += res.second;
    }

    std::cout << "\n\033[1;36mTests summary:\033[0m\n";
    for (auto &&[name, passed]: report) {
        std::cout << "\033[1;34m"
            << name << ": "
            << (passed.first == passed.second ? "\033[0;32m" : "\033[0;31m")
            << passed.first << "\033[0m/\033[0;32m" << passed.second << "\033[0m\n";
    }
}