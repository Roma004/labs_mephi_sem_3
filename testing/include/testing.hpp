#pragma once

#include <exception>
#include <functional>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>
#include <tuple>


class Testing {
public:
    using str = std::string;

    typedef std::function<void ()> test;
    typedef std::vector<std::pair<str, test>> subcategory;
    typedef std::vector<std::pair<str, subcategory>> category;

    class Exception: public std::runtime_error {
    public:
        Exception(str what): std::runtime_error(what) {}
    };

private:

    std::map<str, std::vector<std::tuple<str, str, test>>> ctgs;
    std::vector<str> categories_order; 

    bool process_test(str category_name, str subcategory_name, str name, test t);
    std::pair<int, int> process_category(str name);

public:

    bool has_category(str name) { return ctgs.find(name) != ctgs.end(); }

    void add_category(str name);
    void add_category(str name, category subctgs);
    void add_subcategory(str category_name, str name, subcategory t);
    void add_test(str category_name, str subcategory_name, str name, test t);

    void process();
};
