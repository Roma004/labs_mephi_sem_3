#include <string>
#include <testing.hpp>
#include "tests/test_sequence.hpp"
#include "sequence/Sequence.hpp"
#include "sequence/ListSequence.hpp"
#include "sequence/ArraySequence.hpp"
#include <initializer_list>
#include <vector>

template<class T>
using sequence_category = std::vector<std::pair<std::string, Sequence<T> *>>;

template<class T, template <class> class seq, template <class> class another_seq>
sequence_category<T> get_sequence_category(
    std::initializer_list<T> def,
    std::string seq_type_name,
    std::string another_seq_type_name
) {
    std::vector<T> defaults(def);

    another_seq<T> *temp = new another_seq<T>(def); 

    sequence_category<T> res{
        {"from initializer_list", new seq<T>(def)},
        {"from T*", new seq<T>(defaults.data(), defaults.size())},
    };
    res.push_back({"from "+seq_type_name, new seq<T>(res[0].second)});
    res.push_back({"from "+another_seq_type_name, new seq<T>(temp)});

    delete temp;

    return res;
}

int main() {
    Testing t = Testing();

    std::initializer_list<int> def_int = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::initializer_list<std::string> def_str = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    std::vector<int> defaults_int(def_int);
    std::vector<std::string> defaults_str(def_str);

    ListSequence<int> *default_int_list = new ListSequence<int>;
    ArraySequence<int> *default_int_array = new ArraySequence<int>;
    ListSequence<std::string> *default_string_list = new ListSequence<std::string>;
    ArraySequence<std::string> *default_string_array = new ArraySequence<std::string>;
    
    sequence_category<int> int_lists = get_sequence_category<int, ListSequence, ArraySequence>(
        def_int,
        "ListSequence",
        "ArraySequence"
    );
    sequence_category<int> int_arrays = get_sequence_category<int, ArraySequence, ListSequence>(
        def_int,
        "ArraySequence",
        "ListSequence"
    );
    sequence_category<std::string> string_lists = get_sequence_category<std::string, ListSequence, ArraySequence>(
        def_str,
        "ListSequence",
        "ArraySequence"
    );
    sequence_category<std::string> string_arrays = get_sequence_category<std::string, ArraySequence, ListSequence>(
        def_str,
        "ArraySequence",
        "ListSequence"
    );

    test_sequence_category(
        &t,
        "ListSequence<int> [0; 9]",
        default_int_list,
        int_lists, 
        def_int
    );
    test_sequence_category(
        &t,
        "ArraySequence<int> [0; 9]",
        default_int_array,
        int_arrays, 
        def_int
    );
    test_sequence_category(
        &t,
        "ListSequence<std::string> [0; 9]",
        default_string_list,
        string_lists, 
        def_str
    );
    test_sequence_category(
        &t,
        "ArraySequence<std::string> [0; 9]",
        default_string_array,
        string_arrays, 
        def_str
    );

    t.process();

    for (auto &&[_, seq]: int_lists) delete (ListSequence<int> *)seq;
    for (auto &&[_, seq]: string_lists) delete (ListSequence<std::string> *)seq;

    for (auto &&[_, seq]: int_arrays) delete (ArraySequence<int> *)seq;
    for (auto &&[_, seq]: string_arrays) delete (ArraySequence<std::string> *)seq;

    delete default_int_list;
    delete default_int_array;
    delete default_string_list;
    delete default_string_array;

    return 0;
}