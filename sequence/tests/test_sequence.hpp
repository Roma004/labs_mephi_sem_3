#pragma once

#include "sequence/Sequence.hpp"
#include "testing.hpp"
#include <initializer_list>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>


#define check_result(sequence, _func, args, result) { \
    if (sequence->_func args != result) \
        throw Testing::Exception("result of function `" #_func#args "` is not `" #result "`"); \
}

#define check_error(action, msg) { \
    bool flag = false; \
    try { action; } \
    catch (std::exception) { flag = true; } \
    if (!flag) throw Testing::Exception(msg "  (action: `" #action "`)"); \
}

template <class T>
void check_elements(Sequence<T> *sequence, std::vector<T> elems) {
    std::stringstream ss;
    if (sequence->length() != elems.size()) {
        ss << "size of sequence not matches the correct size: " << sequence->length() << " != " << elems.size() << "\n";
        throw Testing::Exception(ss.str().c_str());
    }
    for (int i = 0; i < sequence->length(); ++i)
        if (sequence->at(i) != elems[i])  {
            ss << "set of elements from sequence gotten (";
            for (int j = 0; j < sequence->length(); ++j) {
                ss << "`" << sequence->at(j) << "` ";
                if (j++ > 30) { ss << "..."; break; }
            }
            ss << ")\ndoes not match the: (";
            for (int j = 0; j < elems.size(); ++j) {
                ss << "`" << elems[j] << "` ";
                if (j++ > 30) { ss << "..."; break; }
            }
            ss << ")\nproblem in position " << i << ":`" << sequence->at(i) << "` != `" << elems[i] << "`\n";
            throw Testing::Exception(ss.str().c_str());
        }
}

template <class T>
void iteration_test(Sequence<T> *sequence, std::vector<T> elems) {
    int ind = 0;
    std::stringstream ss;
    for (auto &i : *sequence) {
        if (i != elems[ind])
            ss << "\n - item `" << i << "` at pos " << ind 
                << " from doesn't match item `" << elems[ind] << "` from initial sequence";
        if (i != sequence->at(ind))
            ss << "\n - item `" << i << "` at pos " << ind 
                << " from doesn't match item `" << sequence->at(ind) << "` from (got by index)";
        ++ind;
    }
    if (!ss.str().empty()) throw Testing::Exception(ss.str().c_str());
}

template <class T>
Testing::subcategory basic(Sequence<T> *sequence, std::vector<T> elems) {
    return {
        {"first", [=]() check_result(sequence, first, (), elems[0]) },
        {"last", [=]() check_result(sequence, last, (), elems[elems.size()-1]) },
        {"length", [=]() check_result(sequence, length, (), elems.size()) },
        {"check all elements", [=]() { check_elements(sequence, elems); }},
        {"iteartion after creation", [=](){ iteration_test(sequence, elems); }},
    };
}

template<class T>
void get_insertion_test_vector(std::vector<T> *dest, std::vector<T> elems, std::vector<T> el) {
    *dest = std::vector<T>(elems);
    dest->insert(dest->begin(), el[0]);
    dest->push_back(el[1]);
    dest->insert(dest->begin(), el[2]);
    dest->push_back(el[3]);
    dest->insert(dest->begin()+dest->size()/2, el[4]);
}

template <class T>
Testing::subcategory insertion(Sequence<T> *sequence, std::vector<T> elems, std::vector<T> el) {
    std::vector<T> res_comp;
    get_insertion_test_vector(&res_comp, elems, el);

    return {
        {"prepend", [=]() { 
            sequence->prepend(el[0]);
            check_result(sequence, first, (), el[0])
            check_result(sequence, at, (0), el[0])
        }},
        {"append", [=]() { 
            sequence->append(el[1]);
            check_result(sequence, last, (), el[1])
            check_result(sequence, at, (sequence->length()-1), el[1])
        }},
        {"insert at 0", [=]() { 
            sequence->insert(el[2], 0);
            check_result(sequence, first, (), el[2])
            check_result(sequence, at, (0), el[2])
        }},
        {"insert at length()", [=]() { 
            sequence->insert(el[3], sequence->length());
            check_result(sequence, last, (), el[3])
            check_result(sequence, at, (sequence->length()-1), el[3])
        }},
        {"insert at length()/2", [=]() { 
            int s = sequence->length();
            sequence->insert(el[4], s/2);
            check_result(sequence, at, (s/2), el[4])
        }},
        {"insert at -1", [=]() check_error(sequence->insert(el[4], -1), "insertion at -1 works!")},
        {"insert at length()+1", [=]() check_error(sequence->insert(el[4], sequence->length()+1), "insertion at size()+1 works!")},
        {"fully check after insertion tests", [=]() { check_elements(sequence, res_comp); }},
        {"iteartion after insertion", [=](){iteration_test(sequence, res_comp); }},
    };
}

template <class T>
Testing::subcategory insertion(Sequence<T> *sequence, std::vector<T> elems) {
    std::vector<T> el(5);
    int k = 0;
    while (k != 5)
        for (int i = 0; i < elems.size() && k != 5; ++i, ++k)
            el[k] = elems[i];
    return insertion(sequence, elems, el);
}

template<class T>
void test_sequence(
    Testing *t,
    Testing::str ctg_name,
    Testing::str seq_name,
    Sequence<T> *sequence,
    std::initializer_list<T> def
) {
    std::vector<T> defaults(def);
    std::stringstream ss, ss1;

    ss << "basic (" << seq_name << ")";
    ss1 << "insertion (" << seq_name << ")";

    if (!t->has_category(ctg_name))
        t->add_category(ctg_name, {
            {ss.str(), basic(sequence, defaults)},
            {ss1.str(), insertion(sequence, defaults)},
        });
    else {
        t->add_subcategory(ctg_name, ss.str(), basic(sequence, defaults));
        t->add_subcategory(ctg_name, ss1.str(), insertion(sequence, defaults));
    }
}

template<class T>
void test_sequence_default(
    Testing *t,
    Testing::str ctg_name,
    Sequence<T> *sequence,
    std::initializer_list<T> def
) {
    std::vector<T> defaults(def);
    std::vector<T> elems(5);
    int k = 0;
    while (k != 5)
        for (int i = 0; i < defaults.size() && k != 5; ++i, ++k)
            elems[k] = defaults[i];

    Testing::subcategory basic_subctg = {
        {"first", [=]() check_error(sequence->first(), "first() works with empty sequence")},
        {"last", [=]() check_error(sequence->last(), "last() works with empty sequence")},
        {"length", [=]() check_result(sequence, length, (), 0) },
        {"check all elements", [=]() {check_elements(sequence, std::vector<T>{});} },
    };
    Testing::subcategory insert_subctg = insertion(sequence, std::vector<T>{}, elems);

    if (!t->has_category(ctg_name))
        t->add_category(ctg_name, {
            {"basic (defaut)", basic_subctg},
            {"insertion (default)", insert_subctg},
        });
    else {
        t->add_subcategory(ctg_name, "basic (defaut)", basic_subctg);
        t->add_subcategory(ctg_name, "insertion (default)", insert_subctg);
    }
}

template<class T>
void test_sequence_category(
    Testing *t,
    Testing::str ctg_name,
    Sequence<T> *default_seq,
    std::vector<std::pair<Testing::str, Sequence<T> *>> sequences,
    std::initializer_list<T> def
) {
    test_sequence_default(t, ctg_name, default_seq, def);
    for (auto &&[seq_name, seq]: sequences) {
        test_sequence(t, ctg_name, seq_name, seq, def);
    }
}