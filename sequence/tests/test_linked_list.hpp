#pragma once

#include "containers/LinkedList.hpp"
#include "testing.hpp"
#include <initializer_list>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>


#define check_result(list, _func, args, result) { \
    if (list->_func args != result) \
        throw Testing::Exception("result of function `" #_func#args "` is not `" #result "`"); \
}

#define check_error(action, msg) { \
    bool flag = false; \
    try { action; } \
    catch (std::exception) { flag = true; } \
    if (!flag) throw Testing::Exception(msg "  (action: `" #action "`)"); \
}

template <class T>
void check_elements(LinkedList<T> *list, std::vector<T> elems) {
    std::stringstream ss;
    if (list->size() != elems.size()) {
        ss << "size of list not matches the correct size: " << list->size() << " != " << elems.size() << "\n";
        throw Testing::Exception(ss.str().c_str());
    }
    for (int i = 0; i < list->size(); ++i)
        if (list->get(i) != elems[i])  {
            ss << "set of elements from list gotten (";
            for (int j = 0; j < list->size(); ++j) {
                ss << "`" << list->get(j) << "` ";
                if (j++ > 30) { ss << "..."; break; }
            }
            ss << ")\ndoes not match the: (";
            for (int j = 0; j < elems.size(); ++j) {
                ss << "`" << elems[j] << "` ";
                if (j++ > 30) { ss << "..."; break; }
            }
            ss << ")\nproblem in position " << i << ":`" << list->get(i) << "` != `" << elems[i] << "`\n";
            throw Testing::Exception(ss.str().c_str());
        }
}

template <class T>
void iteration_test(LinkedList<T> *list, std::vector<T> elems) {
    int ind = 0;
    std::stringstream ss;
    for (auto &i : *list) {
        if (i != elems[ind])
            ss << "\n - item `" << i << "` at pos " << ind 
                << " from LinkedList doesn't match item `" << elems[ind] << "` from initial list";
        if (i != list->get(ind))
            ss << "\n - item `" << i << "` at pos " << ind 
                << " from LinkedList doesn't match item `" << list->get(ind) << "` from LinkedList (got by index)";
        ++ind;
    }
    if (!ss.str().empty()) throw Testing::Exception(ss.str().c_str());
}

template <class T>
Testing::subcategory basic(LinkedList<T> *list, std::vector<T> elems) {
    return {
        {"head", [=]() check_result(list, head, (), elems[0]) },
        {"tail", [=]() check_result(list, tail, (), elems[elems.size()-1]) },
        {"size", [=]() check_result(list, size, (), elems.size()) },
        {"check all elements", [=]() { check_elements(list, elems); }},
    };
}

template<class T>
void get_insertion_test_vector(std::vector<T> *dest, std::vector<T> elems, std::vector<T> el) {
    *dest = std::vector<T>(elems);
    dest->insert(dest->begin(), el[0]);
    dest->push_back(el[1]);
    dest->insert(dest->begin(), el[2]);
    dest->push_back(el[3]);
    dest->insert(dest->begin()+dest->size()/2, el[4]);
}

template <class T>
Testing::subcategory insertion(LinkedList<T> *list, std::vector<T> elems, std::vector<T> el) {
    std::vector<T> res_comp;
    get_insertion_test_vector(&res_comp, elems, el);

    return {
        {"addl", [=]() { 
            list->addl(el[0]);
            check_result(list, head, (), el[0])
            check_result(list, get, (0), el[0])
        }},
        {"addr", [=]() { 
            list->addr(el[1]);
            check_result(list, tail, (), el[1])
            check_result(list, get, (list->size()-1), el[1])
        }},
        {"insert at 0", [=]() { 
            list->insert(el[2], 0);
            check_result(list, head, (), el[2])
            check_result(list, get, (0), el[2])
        }},
        {"insert at size()", [=]() { 
            list->insert(el[3], list->size());
            check_result(list, tail, (), el[3])
            check_result(list, get, (list->size()-1), el[3])
        }},
        {"insert at size()/2", [=]() { 
            int s = list->size();
            list->insert(el[4], s/2);
            check_result(list, get, (s/2), el[4])
        }},
        {"insert at -1", [=]() check_error(list->insert(el[4], -1), "insertion at -1 works!")},
        {"insert at size()+1", [=]() check_error(list->insert(el[4], list->size()+1), "insertion at size()+1 works!")},
        {"fully check after insertion tests", [=]() { check_elements(list, res_comp); }},
    };
}

template <class T>
void test_linked_list(Testing *t, Testing::str ctg_name, std::initializer_list<T> def) {
    std::vector<T> defaults(def);
    
    LinkedList<T> *list = new LinkedList<T>;
    LinkedList<T> *list_def = new LinkedList<T>(def);
    LinkedList<T> *list_list = new LinkedList<T>(list_def);
    
    T items[defaults.size()] = {};
    for (int i = 0; i < defaults.size(); ++i) items[i] = defaults[i];

    LinkedList<T> *list_items = new LinkedList<T>(items, defaults.size());

    // basic tests
    t->add_category(ctg_name, {
        {"basic (empty list)", {
            {"head", [=]() check_error(list->head(), "head() works with empty list")},
            {"tail", [=]() check_error(list->tail(), "tail() works with empty list")},
            {"size", [=]() check_result(list, size, (), 0) },
            {"check all elements", [=]() {check_elements(list, std::vector<T>{});} },
        }},
        {"basic (from initializer_list)", basic(list_def, defaults)},
        {"basic (from LinkedList)", basic(list_list, defaults)},
        {"basic (from T*)", basic(list_items, defaults)},
    });


    // iteration after creation
    t->add_subcategory(ctg_name, "iteration after cteation", {
        {"from empty list", [=](){iteration_test(list, std::vector<T>{});}},
        {"from initializer_list", [=](){iteration_test(list_def, defaults);}},
        {"from LinkedList", [=](){iteration_test(list_list, defaults);}},
        {"from T*", [=](){iteration_test(list_items, defaults);}},
    });


    std::vector<T> elems(5);
    int k = 0;
    while (k != 5) for (int i = 0; i < defaults.size() && k != 5; ++i, ++k) elems[k] = defaults[i];

    // insertion tests
    t->add_subcategory(
        ctg_name, "insertion (empty list)",
        insertion(list, std::vector<T>{}, elems)
    );
    t->add_subcategory(ctg_name, "insertion (from initializer_list)",
        insertion(list_def, defaults, elems)
    );
    t->add_subcategory(
        ctg_name, "insertion (from LinkedList)",
        insertion(list_list, defaults, elems)
    );
    t->add_subcategory(
        ctg_name, "insertion (from T*)",
        insertion(list_items, defaults, elems)
    );

    const int new_size = 5+defaults.size();
    std::vector<T> insertion_res, insertion_empty_res;
    get_insertion_test_vector(&insertion_res, defaults, elems);
    get_insertion_test_vector(&insertion_empty_res, std::vector<T>(), elems);

    // iteration after insertion
    t->add_subcategory(ctg_name, "iteration after insertion", {
        {"from empty list", [=](){iteration_test(list, insertion_empty_res);}},
        {"from initializer_list", [=](){iteration_test(list_def, insertion_res);}},
        {"from LinkedList", [=](){iteration_test(list_list, insertion_res);}},
        {"from T*", [=](){iteration_test(list_items, insertion_res);}},
    });
}