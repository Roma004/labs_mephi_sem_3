#include <testing.hpp>
#include "tests/test_linked_list.hpp"
#include <initializer_list>
#include <vector>

int main() {
    Testing t = Testing();

    std::initializer_list<int> def_int = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::initializer_list<std::string> def_str = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    test_linked_list(&t, "LinkedList<int> [0; 9]", def_int);
    test_linked_list(&t, "LinkedList<std::string> [str(i) for i in 0..9]", def_str);

    t.process();
}