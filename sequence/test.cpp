#include <iostream>
#include <ostream>

#include "sequence/ListSequence.hpp"
#include "quick_sort.hpp"
#include "selection_sort.hpp"
#include "binary_insertion_sort.hpp"

int main() {
    ListSequence<int> *l = new ListSequence<int>{10, 1, 0, 2, 3, 7, 7, 0, 3, 2, 0, 11};
    ListSequence<int> *l1 = new ListSequence<int>(l);
    ListSequence<int> *l2 = new ListSequence<int>(l);

    std::cout << "l:  " << std::flush;
    for (auto &i : *l) {
        std::cout << i << " ";
    }
    std::cout << "\n";

    quick_sort(l->begin(), l->end());
    selection_sort(l1->begin(), l1->end());
    binary_insertion_sort(l2->begin(), l2->end());

    std::cout << "l:  " << std::flush;
    for (auto &i : *l) {
        std::cout << i << " ";
    }
    std::cout << "(quick)\n";

    std::cout << "l1: " << std::flush;
    for (auto &i : *l1) {
        std::cout << i << " ";
    }
    std::cout << "(selection)\n";

    std::cout << "l2: " << std::flush;
    for (auto &i : *l2) {
        std::cout << i << " ";
    }
    std::cout << "(binary insertion)\n";

    return 0;
}