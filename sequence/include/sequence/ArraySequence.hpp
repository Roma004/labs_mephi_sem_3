#pragma once

#include "containers/DynamicArray.hpp"
#include "containers/ContainerIterator.hpp"
#include "Sequence.hpp"
#include <cstdio>
#include <initializer_list>

template<class T>
class ArraySequence: public Sequence<T> {
protected:
    DynamicArray<T> data;

public:
    typedef ContainerIterator<T> iterator;
    
    ArraySequence(): data(DynamicArray<T>(0)) {}
    ArraySequence(T *items, int number): data(DynamicArray<T>(items, number)) {}
    ArraySequence(IterableContainer<T> *c): data(DynamicArray(c)) {}

    ArraySequence(std::initializer_list<T> list): data(DynamicArray<T>(list)) {}

    int length() override { return data.size(); }
    T &at(int index) override { return data[index]; }
    void insert(T item, int index) override;

    void erase(int from, int to) override;

    void set_length(int length) override { data.resize(length); }

    iterator begin() override { return data.begin(); }
    iterator end() override { return data.end(); }

    void *__next__(void *it) override { return data.__next__(it); }
    void *__prev__(void *it) override { return data.__prev__(it); }
    T &__deref__(void *it) override { return data.__deref__(it); }
};

/////////////////////// ARRAY_WRAPPER_IMPLEMENTATIONS  ////////////////////////

template<class T>
void ArraySequence<T>::insert(T item, int index) {
    if (index < 0 || index > length()) {
        throw std::runtime_error("index out of range");
    }
    
    data.resize(length() + 1);

    for (size_t i = length()-1; i > index; --i) {
        data[i] = data[i-1];
    }

    data[index] = item;
}

template<class T>
void ArraySequence<T>::erase(int from, int to) {
    if (from < 0 || length() <= from || to < 0 || length() < to || from >= to) {
        throw std::runtime_error("index out of range");
    }

    int delta = to-from;

    for (size_t i = to; i < length(); ++i) {
        data[i-delta] = data[i];
    }

    data.resize(length()-delta);
}