#pragma once

#include "Sequence.hpp"
#include "containers/ContainerIterator.hpp"
#include "containers/LinkedList.hpp"
#include "sequence/SequenceIterator.hpp"

template <class T>
class ListSequence: public Sequence<T> {
private:
    LinkedList<T> list;

public:
    using typename Sequence<T>::iterator;

    ListSequence(): list(LinkedList<T>()) {}
    ListSequence(T *items, int number): list(LinkedList<T>(items, number)) {}
    ListSequence(IterableContainer<T> *c): list(LinkedList(c)) {}

    ListSequence(std::initializer_list<T> list): list(LinkedList<T>(list)) {}

    int length() override { return list.size(); }
    T &at(int index) override { return list[index]; };
    void insert(T item, int index) override { list.insert(item, index); }

    void erase(int from, int to) override { list.erase(from, to); }

    void set_length(int length) override;

    iterator begin() override { return list.begin(); }
    iterator end() override { return list.end(); }

    void *__next__(void *it) override { return list.__next__(it); }
    void *__prev__(void *it) override { return list.__prev__(it); }
    T &__deref__(void *it) override { return list.__deref__(it); }
};

template<class T>
void ListSequence<T>::set_length(int length) {
    if (length < this->length()) {
        erase(length, this->length());
    } else {
        for (int i = this->length(); i < length; ++i) {
            T a = T();
            this->append(a);
        }
    }
}
