#pragma once

#include "containers/ContainerIterator.hpp"
#include <iterator>


template <class T>
using SequenceIterator = ContainerIterator<T>;