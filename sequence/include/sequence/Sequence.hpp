#pragma once


#include "containers/ContainerIterator.hpp"
#include "sequence/SequenceIterator.hpp"
template <class T>
class Sequence: public IterableContainer<T> {
public:
    virtual int length() = 0;
    virtual T &at(int index) = 0;
    virtual void insert(T item, int index) = 0;
    virtual void erase(int from, int to) = 0;
    // virtual void erase(iterator &from, iterator &to) = 0;

    virtual void set_length(int length) = 0;

    T &first() { return at(0); }
    T &last() { return at(length()-1); }

    T &operator[](int index) { return at(index); }

    virtual void del(int index) { erase(index, index+1); };
    void del_first() { del(0); }
    void del_last() { del(length()); }

    void clear() { erase(0, length()); }
    void append(T item) {insert(item, length());}
    void prepend(T item) {insert(item, 0);}
};