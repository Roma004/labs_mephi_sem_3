#pragma once

#include <cstddef>

#include <iterator>

template <class T>
class IterableContainer;

template <class T>
class ContainerIterator {
    typedef ContainerIterator<T> _self;
    typedef IterableContainer<T> * _container;
protected:
    void *p;
    _container c;

public:
    ContainerIterator(_container c): p(NULL), c(c) {}
    ContainerIterator(const _container c, void* p) {
        this->c = c;
        this->p = p;
    }

    ContainerIterator(const _self &it): p(it.p), c(it.c) {}
    ContainerIterator(_self &it): p(it.p), c(it.c) {}

    _self &operator=(_self &other) {
        this->c = other.c;
        this->p = other.p;
        return *this;
    }

    bool operator!=(_self const& other) const { return p != other.p; }
    bool operator==(_self const& other) const { return p == other.p; }
    virtual T &operator*() const { return c->__deref__(p); };
    virtual _self& operator++() { p = c->__next__(p); return *this; }
    virtual _self& operator--() { p = c->__prev__(p); return *this; }
};


template <class T>
class IterableContainer {
public:
    typedef ContainerIterator<T> iterator;
    virtual iterator begin() = 0;
    virtual iterator end() = 0;
    
    virtual void *__next__(void *) = 0;
    virtual void *__prev__(void *) = 0;
    virtual T &__deref__(void *) = 0;
};
