#pragma once

#include <cstddef>
#include <initializer_list>
#include <sstream>
#include <stdexcept>
#include "ContainerIterator.hpp"

/////////////////////////// LINKED LIST ////////////////////////////

template <class T>
class LinkedList: public IterableContainer<T> {
public:
    typedef ContainerIterator<T> iterator;

private:

    struct item_wrapper;

    item_wrapper *list_head = nullptr;
    item_wrapper *list_tail = nullptr;
    int list_size = 0;

    item_wrapper *get_ptr_by_index(int index);

    void __erase(item_wrapper *from_ptr, item_wrapper *to_ptr);

public:
    
    LinkedList(T *items, int number);
    LinkedList()
        : list_head(nullptr)
        , list_tail(nullptr)
        , list_size(0) {}
    LinkedList(IterableContainer<T> *);
    LinkedList(std::initializer_list<T> list);

    ~LinkedList();

    T &head() { 
        if (list_head != nullptr) return list_head->value;
        throw std::out_of_range("head is unavailable with empty list");
    }
    T &tail() { 
        if (list_head != nullptr) return list_tail->value;
        throw std::out_of_range("tail is unavailable with empty list");
    }
    int size() { return list_size; }

    void addl(T item);
    void addr(T item);

    void insert(T item, int index);

    void erase(int from, int to);
    void erase(iterator &from, iterator &to);

    T &get(int index) { return this->get_ptr_by_index(index)->value; }

    T &operator[](int index) { return this->get_ptr_by_index(index)->value; }

    iterator begin() override { return iterator(this, (void*)list_head); }
    iterator end() override { return iterator(this); }

    void *__next__(void *it) override { return (void *)(((item_wrapper*)it)->next); }
    void *__prev__(void *it) override {
        if (it == nullptr) return (void *)list_tail;
        return (void *)(((item_wrapper*)it)->prev);
    }
    T &__deref__(void *it) override { return ((item_wrapper*)it)->value; }
};


/////////////////////////// ITEM WRAPPER ///////////////////////////

template <class T>
struct LinkedList<T>::item_wrapper {
    T value;
    item_wrapper *prev;
    item_wrapper *next;

    item_wrapper(T value, item_wrapper *prev, item_wrapper *next) 
        : value(value)
        , prev(prev)
        , next(next) {}
};


/////////////////////////// CONSTRUCTORS ///////////////////////////

template <class T>
LinkedList<T>::LinkedList(T *items, int number) {
    for (T *it = items; it < items + number; ++it) {
        this->addr(*it);
    }
}

template<class T>
LinkedList<T>::LinkedList(IterableContainer<T> *c) {
    for (auto &it : *c) {
        this->addr((T &)it);
    }
}

template<class T>
LinkedList<T>::LinkedList(std::initializer_list<T> list) {
    for (auto &it : list) {
        this->addr((T &)it);
    }
}

//////////////////////////// DESTRUCTOR ////////////////////////////

template<class T>
LinkedList<T>::~LinkedList() {
    while (list_tail) {
        item_wrapper *tmp = list_tail->prev;
        delete list_tail;
        list_tail = tmp;
    }
    list_size = 0;
}

///////////////////////////// METHODS //////////////////////////////

template<class T>
void LinkedList<T>::addl(T item) {
    if (list_size == 0) {
        list_head = new item_wrapper(item, nullptr, nullptr);
        list_tail = list_head;
        list_size = 1;

        return;
    }

    list_head->prev = new item_wrapper(item, nullptr, list_head);
    list_head = list_head->prev;

    ++list_size;
}

template<class T>
void LinkedList<T>::addr(T item) {
    if (list_size == 0) {
        list_head = new item_wrapper(item, nullptr, nullptr);
        list_tail = list_head;
        list_size = 1;

        return;
    }

    list_tail->next = new item_wrapper(item, list_tail, nullptr);
    list_tail = list_tail->next;

    ++list_size;
}

template<class T>
typename LinkedList<T>::item_wrapper *LinkedList<T>::get_ptr_by_index(int index) {
    if (index < 0 || index >= list_size) {
        std::stringstream ss;
        ss << "list index out of range (maximal allowed index is " << list_size-1 << ", target index is " << index;
        throw std::out_of_range(ss.str()); 
    }

    item_wrapper *res;
    if (index < list_size / 2) {
        res = list_head;
        for (int i = 0; i < index; ++i) {
            res = res->next;
        }
    } else {
        res = list_tail;
        for (int i = list_size-1; i > index; --i) {
            res = res->prev;
        }
    }

    return res;
}

template<class T>
void LinkedList<T>::insert(T item, int index) {
    if (index < 0 || index > list_size) {
        std::stringstream ss;
        ss << "list index out of range (maximal allowed index is " << list_size << ", target index is " << index;
        throw std::out_of_range(ss.str()); 
    }

    if (index == 0) {
        this->addl(item);
        return;
    }

    if (index == list_size) {
        this->addr(item);
        return;
    }

    item_wrapper *tmp = this->get_ptr_by_index(index-1);

    tmp->next = new item_wrapper(item, tmp, tmp->next);
    tmp->next->next->prev = tmp->next;

    ++list_size;
}


template <class T>
void LinkedList<T>::erase(int from, int to) {
    item_wrapper *from_ptr = get_ptr_by_index(from);
    item_wrapper *to_ptr = get_ptr_by_index(to);
    __erase(from_ptr, to_ptr);
}

template <class T>
void LinkedList<T>::erase(iterator &from, iterator &to) {
    item_wrapper *from_ptr = from.p;
    item_wrapper *to_ptr = to.p;
    __erase(from_ptr, to_ptr);
}

template <class T>
void LinkedList<T>::__erase(item_wrapper *from_ptr, item_wrapper *to_ptr) {
    item_wrapper *prev = from_ptr->prev;

    int deleted = 0;
    while (from_ptr < to_ptr) {
        item_wrapper *tmp = from_ptr->next;
        delete from_ptr;
        from_ptr = tmp;

        ++deleted;
    }

    if (prev) prev->next = from_ptr;
    else list_head = from_ptr;

    if (from_ptr) from_ptr->prev = prev;
    else list_tail = prev;

    list_size -= deleted;
}
