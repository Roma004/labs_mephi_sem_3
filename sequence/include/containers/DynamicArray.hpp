#pragma once

#include <algorithm>
#include <initializer_list>
#include <stdexcept>
#include <cstring>
#include "ContainerIterator.hpp"

/////////////////////////////// DYNAMIC ARRAY ///////////////////////////////

template <class T>
class DynamicArray: public IterableContainer<T> {
private:
    T *data = nullptr;
    int array_size = 0;

public:
    typedef ContainerIterator<T> iterator;

    DynamicArray(T *items, int number);
    DynamicArray(int size);
    DynamicArray(): DynamicArray(0) {}
    DynamicArray(const DynamicArray<T> &array);
    DynamicArray(IterableContainer<T> *c);
    DynamicArray(std::initializer_list<T> list);

    ~DynamicArray();

    int size();

    void resize(int new_size);

    T &operator[](int index);

    iterator begin() override { return iterator(this, data); }
    iterator end() override { return iterator(this, data + array_size); }

    void *__next__(void *it) override { T *t = (T *)it; return ++t; }
    void *__prev__(void *it) override { T *t = (T *)it; return --t; }
    T &__deref__(void *it) override { return *((T *)it); }
};

//////////////////////////////// CONSTRUCTORS ////////////////////////////////

template<class T>
DynamicArray<T>::DynamicArray(int size) {
    if (size < 0) {
        throw std::runtime_error("negative array size");
    }

    if (size == 0) return;

    data = new T[size];
    memset(data, 0, size*sizeof(T));
    array_size = size;
}

template<class T>
DynamicArray<T>::DynamicArray(T *items, int number) {
    if (number < 0) {
        throw std::runtime_error("negative array size");
    }

    if (number == 0) return;
    
    data = new T[number];
    std::copy(items, items + number, data);
    array_size = number;
}

template<class T>
DynamicArray<T>::DynamicArray(const DynamicArray<T> &array) {
    data = new T[array.array_size];
    std::copy(array.data, array.data + array.array_size, data);

    array_size = array.array_size;
}

template<class T>
DynamicArray<T>::DynamicArray(IterableContainer<T> *c) {
    int idx = 0, len = 0;
    for (auto &it: *c) ++len;
    resize(len);

    for (auto &it: *c) data[idx++] = it;
}



template<class T>
DynamicArray<T>::DynamicArray(std::initializer_list<T> list) {
    data = new T[list.size()];
    int i = 0;
    for (auto &it : list) {
        data[i++] = it;
    }

    array_size = list.size();
}

///////////////////////////////// DESTRUCTOR /////////////////////////////////

template<class T>
DynamicArray<T>::~DynamicArray() {
    delete [] data;
    array_size = 0;
}

/////////////////////////// METHODS IMPLEMENTATION ///////////////////////////

template<class T>
int DynamicArray<T>::size() {
    return array_size;
}

template<class T>
void DynamicArray<T>::resize(int new_size) {
    if (new_size < 0) {
        throw std::runtime_error("negative array size");
    }
    T *tmp = data;
    data = new T[new_size];

    std::copy(tmp, tmp + std::min(array_size, new_size), data);
    array_size = new_size;
    
    delete [] tmp;
}

template<class T>
T &DynamicArray<T>::operator[](int index) {
    if (index < 0 || index >= array_size) {
        throw std::runtime_error("index out of range");
    }
    
    return data[index];
}
