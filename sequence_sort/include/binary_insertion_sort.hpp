#pragma once
#include "__defs.hpp"
#include <vector>


template <class T>
void binary_insertion_sort(_iter<T> l, _iter<T> r, _cmp<T> lt) {
    std::vector<_iter<T>> iters;
    for (auto i(l); i != r; ++i) iters.push_back(i);

    for (int i = 1; i != iters.size(); ++i) {
        _iter<T> val = iters[i];
        int start = -1, end = i;

        while (end - start > 1) {
            int mid = (start + end) / 2;

            if (__le(*(iters[mid]), *val, lt)) start = mid;
            else end = mid;
        }

        if (!__eq(*(iters[i]), *(iters[start + 1]), lt)) {
            cycle_shift(iters[start], iters[i+1]);
        }
    }
}

template <class T>
void binary_insertion_sort(_iter<T> l, _iter<T> r) {
    binary_insertion_sort(l, r, __default_lt<T>);
}
