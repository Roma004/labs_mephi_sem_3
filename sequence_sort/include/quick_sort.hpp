#pragma once

#include "__defs.hpp"

template <class T>
_iter<T> get_sort_segment(_iter<T> l, _iter<T> r, _cmp<T> lt) {
    _iter<T> end(r); --end;
    auto p = *end;
    _iter<T> i(l);

    for (auto j(l); j != end; ++j) {
        if (lt(*j, p)) {
            swap(i, j);
            ++i;
        }
    }

    swap(end, i);
    return i;
}

template <class T>
void quick_sort(_iter<T> l, _iter<T> r, _cmp<T> lt) {
    auto q = get_sort_segment(l, r, lt);
    auto q1(q); ++q1;

    if (l != q) quick_sort(l, q, lt);
    if (r != q1) quick_sort(q1, r, lt);
}

template <class T>
void quick_sort(_iter<T> l, _iter<T> r) {
    quick_sort(l, r, __default_lt<T>);
}
