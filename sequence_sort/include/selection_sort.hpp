#pragma once

#include "__defs.hpp"

template<class T>
void selection_sort(_iter<T> l, _iter<T> r, _cmp<T> lt) {
    for (auto i(l); i != r; ++i) {
        _iter<T> min(i);
        for (auto j(i); j != r; ++j)
            if (lt(*j, *min)) min = j;
        if (min != i) swap(min, i);
    }
}

template<class T>
void selection_sort(_iter<T> l, _iter<T> r) {
    selection_sort(l, r, __default_lt<T>);
}