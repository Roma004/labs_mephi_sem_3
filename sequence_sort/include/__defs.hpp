#pragma once

#include "sequence/SequenceIterator.hpp"
#include <functional>

template<class T>
using _iter = SequenceIterator<T>;

template<class T>
using _cmp = std::function<bool(T &, T &)>;

template <class T>
void swap(_iter<T> &a, _iter<T> &b) {
    T tmp = *a;
    *a = *b;
    *b = tmp;
}

template<class T>
void cycle_shift(_iter<T> &start, _iter<T> &end) {
    _iter<T> begin(end); --begin;
    _iter<T> i(begin); --begin;
    for (; begin != start; --begin, --i) {
        swap(i, begin);
    }
}

template <class T>
bool __default_lt(T &a, T &b) {
    return a < b;
}

template <class T>
bool __eq(T &a, T &b, std::function<bool(T &, T &)> lt) {
    return !lt(a, b) && !lt(b, a);
}

template <class T>
bool __le(T &a, T &b, std::function<bool(T &, T &)> lt) {
    return lt(a, b) || __eq(a, b, lt);
}